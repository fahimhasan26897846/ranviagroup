<?php /*Template Name: Single Page*/ ?>
<?php get_header(); ?>
    <section class="banner-page">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 offset-md-3">
                    <h2 class="banner-head-content text-light text-center animated  fadeInDown ">Bringing innovative digital health solutions to patient care...</h2>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container ">
            <div class="p-5">
                <?php while (have_posts()) : the_post(); ?>
                    <div class="m-5">
                        <div class="row">
                            <h3 class="text-warning default-family" style="text-transform: uppercase"><?php the_title(); ?></h3>
                        </div>
                        <div class="p-4">
                            <div>
                                <p class="paragraph default-family extra-color pt-4"><?php the_content() ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>

            </div>
        </div>
    </section>
<?php get_footer(); ?>