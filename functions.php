<?php

add_theme_support('title-tag');

add_theme_support('post-thumbnails');
set_post_thumbnail_size( 450);


register_nav_menu('main','Main menu bar');


register_nav_menu('footermenu','Footer menu bar');

if ( ! file_exists( get_template_directory() . '/class-wp-bootstrap-navwalker.php' ) ) {
    // file does not exist... return an error.
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
    // file exists... require it.
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

if(file_exists(dirname(__FILE__).'/inc/class-tgm-plugin-activation.php')){
    require_once(dirname(__FILE__).'/inc/class-tgm-plugin-activation.php');
}

if(file_exists(dirname(__FILE__).'/inc/example.php')){
    require_once(dirname(__FILE__).'/inc/example.php');
}

add_filter("the_content", "plugin_myContentFilter");

function plugin_myContentFilter($content)
{
    // Take the existing content and return a subset of it
    return substr($content, 0, 50000);
}

add_filter("the_excerpt", "plugin_myContentFilte");

function plugin_myContentFilte($contents)
{
    // Take the existing content and return a subset of it
    return substr($contents, 0, 100);
}


?>