<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri()?>/resources/images/ranvia.png">
    <title>Ranvia Group</title>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/resources/css/animate.css">
    <link href="<?php echo get_template_directory_uri()?>/resources/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri()?>/resources/css/owl.theme.default.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php bloginfo('template_directory')?>/style.css">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="preloader-container">
    <div class="preloader-1">
        <div>Loading</div>
        <span class="line line-1"></span>
        <span class="line line-2"></span>
        <span class="line line-3"></span>
        <span class="line line-4"></span>
        <span class="line line-5"></span>
        <span class="line line-6"></span>
        <span class="line line-7"></span>
        <span class="line line-8"></span>
        <span class="line line-9"></span>
    </div>
</div>
<nav class="navbar navbar-expand-lg fixed-top navbar-light">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri()?>/resources/images/logo.png" id="logomain" alt="ranviar group logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-navicon text-light"></span>
        </button>
        <div class="collapse navbar-collapse  justify-content-end" id="navbarNavDropdown">
            <?php wp_nav_menu(array(
                'theme_location' => 'main',
                'depth'				=> 1, // 1 = with dropdowns, 0 = no dropdowns.
                'menu_class'		=> 'navbar-nav mr-auto',
                'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
                'walker'			=> new WP_Bootstrap_Navwalker()
            )) ?>

        </div>
    </div>
</nav>
