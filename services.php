<?php /*Template Name: SERVICES*/ ?>
<?php get_header(); ?>
    <section class="about-consultants p-4" id="medical-content-page">
        <div class="container justify-content-center">
            <h2 class="text-light text-center animated  fadeInDown camelcase">Medical Content</h2>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row text-md-center mt-5">
                <div class="col-md-6">
                    <img src="<?php echo get_template_directory_uri()?>/resources/images/react.png" alt="clinic" class="img-fluid mb-5">
                    <h3 class="warn-color default-family">Scientific and medical writing</h3>
                    <p class="paragraph mb-lg-5"> Creation of content for whitepapers <br>
                         Article and medical blog content creation & editing <br>
                         Health reports & newsletters <br>
                         Seo and keyword optimization</p>
                </div>
                <div class="col-md-6"><img src="<?php echo get_template_directory_uri()?>/resources/images/erm-software.png" alt="clinic" class="img-fluid mb-5">
                    <h3 class="warn-color default-family">Brand development & marketing</h3>
                    <p class="paragraph mb-lg-5"> We can help you bring your vision for your brand to life
                       <br>
                        Reach and engage your audience with customized content for your product or service</p>
                </div>
            </div>
        </div>
    </section>
<section class="banner-page" id="banner">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 offset-md-3">
                <h2 class="banner-head-content text-light text-center animated  fadeInDown camelcase">Medical Consulting Services</h2>
            </div>
        </div>
    </div>
</section>
      <section>
          <div class="container">
              <div class="row text-md-center mt-5">
              <div class="col-md-6">
                  <img src="<?php echo get_template_directory_uri()?>/resources/images/clinic.png" alt="clinic" class="img-fluid mb-5">
                  <h3 class="warn-color default-family ">Clinical needs
                      assessment</h3>
                  <p class="paragraph mb-lg-5"> We will assess your clinical workflow for areas of need <br>
                       Provide recommendations based on client expectations</p>
              </div>
              <div class="col-md-6"><img src="<?php echo get_template_directory_uri()?>/resources/images/erm-software.png" alt="clinic" class="img-fluid mb-5">
                  <h3 class="warn-color default-family">EMR & software
                      testing</h3>
                  <p class="paragraph mb-lg-5"> At-the-elbow support (for GO-LIVES) <br>
                       Testing new EMR implementations</p>
              </div>
          </div>
              <div class="row text-md-center mt-5">
                  <div class="col-md-6">
                      <img src="<?php echo get_template_directory_uri()?>/resources/images/tie.png" alt="clinic" class="img-fluid mb-5">
                      <h3 class="warn-color default-family">Executive
                          leadership</h3>
                      <p class="paragraph mb-lg-5">Available for clinical cofounder role with startups in the
                          medical & biotech industry.</p>
                  </div>
                  <div class="col-md-6"><img src="<?php echo get_template_directory_uri()?>/resources/images/research.png" alt="clinic" class="img-fluid mb-5">
                      <h3 class="warn-color default-family">Market research</h3>
                      <p class="paragraph mb-lg-5"> We provide a wide-range of services in the area
                         of market research. Contact us today!</p>
                  </div>
              </div>
          </div>
      </section>
<?php get_footer(); ?>