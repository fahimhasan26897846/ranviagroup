<?php /*Template Name: Blog*/ ?>
<?php get_header(); ?>
      <section class="banner-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 offset-md-3">
                <h2 class="banner-head-content text-light text-center animated  fadeInDown camelcase">Bringing innovative digital health solutions to patient care...</h2>
            </div>
        </div>
    </div>
</section>
      <section>
          <div class="container bottom-border border-left-0 border-right-0 border-top-0">
              <div class="p-2 d-flex flex-wrap">
                  <?php while (have_posts()) : the_post(); ?>
                  <div class="blogpost m-5">
                  <div class="thumbnail-div">
                      <h3 class="camelcase "><?php the_title(); ?></h3>
                      <?php if(has_post_thumbnail()){the_post_thumbnail('', array('class' => 'img-fluid dodo'));
                      } else { ?>
                      <img src="<?php bloginfo('template_directory'); ?>/resources/images/thumbnail.jpg" alt="<?php the_title(); ?>" class="img-fluid dodo" />
                      <?php } ?>

                  </div>
                  <div class="card-content p-4">
                      <div class="row bottom-border border-left-0 border-right-0 border-top-0">
                          <div class="col-md-6"><p class="paragraph">Posted by <span class="color">admin</span></p></div>
                      </div>
                      <div>
                          <p class="paragraph pt-4"><?php the_excerpt() ?></p>
                      </div>
                      <div class="row bottom-border border-right-0 border-bottom-0 border-left-0">
                          <div class="col-sm-6">
                              <div class="d-flex">
                                  <i class="fa fa-facebook color pl-3 pt-3 pb-3"></i><i
                                      class="fa fa-twitter color pl-3 pt-3 pb-3"></i><i
                                      class="fa fa-google-plus color pl-3 pt-3 pb-3"></i><i
                                      class="fa fa-linkedin color pl-3 pt-3 pb-3"></i><i
                                      class="fa fa-pinterest color pl-3 pt-3 pb-3"></i>
                              </div>
                          </div>
                          <div class="col-sm-6 p-3 text-lg-right"><a href="<?php the_permalink() ?>" class="btn btn-sm warn-bg">READ MORE</a></div>


                      </div>
                  </div>
              </div>
                  <?php endwhile; ?>

              </div>
              <div class="pagination">
              <?php the_posts_pagination(array(
                      'prev_text' => 'PREV',
                      'next_text' => 'NEXT',
                      'screen_reader_text' => ' '
              )) ?>
              </div>
          </div>
          <div class="container">
              <div class="p-5">
                  <div class="m-5 testimoniel-corusel">

                  </div>
              </div>
          </div>
      </section>
      <section class="testimonial-slider">

          <div class="container text-md-center">
              <h3 class="default-family color mb-5 camelcase">Recent Testimonials</h3>
              <div class="item bg-gray p-5 m-5">
                  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                      <div class="carousel-item active">
                          <div class="row">
                              <div class="col-md-3">
                                  <img src="<?php echo get_template_directory_uri()?>/resources/images/testimonial.jpg" alt="testimonial" class="rounded-circle mb-3 mt-3">
                              </div>
                              <div class="col-md-9">
                                  <p class="paragraph">Matthew’s team’s articles on sports psychology were thorough, informative and easy to read. I will be working with them again in the future!</p>
                                  <div class="text-right">
                                      <h6 class="paragraph text-dark">ALEXIA</h6>
                                      <p class="paragraph">Scientific Writing Client</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="carousel-item">
                          <div class="row">
                              <div class="col-md-3">
                                  <img src="<?php echo get_template_directory_uri()?>/resources/images/testimonial.jpg" alt="testimonial" class="rounded-circle mb-3 mt-3">
                              </div>
                              <div class="col-md-9">
                                  <p class="paragraph">Matthew’s team’s articles on sports psychology were thorough, informative and easy to read. I will be working with them again in the future!</p>
                                  <div class="text-right">
                                      <h6 class="paragraph text-dark">ALEXIA</h6>
                                      <p class="paragraph">Scientific Writing Client</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="carousel-item">
                          <div class="row">
                              <div class="col-md-3">
                                  <img src="<?php echo get_template_directory_uri()?>/resources/images/testimonial.jpg" alt="testimonial" class="rounded-circle mb-3 mt-3">
                              </div>
                              <div class="col-md-9">
                                  <p class="paragraph">Matthew’s team’s articles on sports psychology were thorough, informative and easy to read. I will be working with them again in the future!</p>
                                  <div class="text-right">
                                      <h6 class="paragraph text-dark">ALEXIA</h6>
                                      <p class="paragraph">Scientific Writing Client</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                  </a>
              </div>
              </div>

          </div>
      </section>
      <section class="digital-health-market">
          <div class="container text-center pt-5">
              <h3 class="color mt-5 mb-5 default-family">Digital Health Market Reports</h3>
              <div class="row pt-5 pb-5">
                  <div class="col-md-6 p-3">
                      <div class="bottom-border rounded p-5">
                      <img src="<?php echo get_template_directory_uri()?>/resources/images/bookone.jpg" class="pb-5" alt="book">
                      <p class="text-light pb-5 default-family">
                          Trickle-up Approach to Digital Health Product Commercialization Available to the general audience Feb 22 || Price: US $650 If you are interested in purchase, please email us at
                          contact@ranviagroup.com
                      </p>
                      </div>
                  </div>
                  <div class="col-md-6 p-3">
                      <div class="bottom-border rounded p-5">
                      <img src="<?php echo get_template_directory_uri()?>/resources/images/booktwo.jpg" class="pb-5" alt="book">
                      <p class="text-light default-family pb-5">
                          A Physicians guide to Direct-to-Consumer Genomic Tests Available to the general audience March 24 || Price: US $650 If you are interested in purchase, please email us at contact@ranviagroup.com
                      </p>
                      </div>
                  </div>
              </div>
          </div>
      </section>
<?php get_footer(); ?>