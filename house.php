<?php /*Template Name: Home*/ ?>
<?php get_header(); ?>
      <section class="banner">
        <div class="container ">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner align-items-center">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <h3 class="banner-content text-light text-center"> Providing consistent, evidence-based medical content for a variety of applications</h3>
                            </div>
                        </div>

                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <h3 class="banner-content text-light text-center"> Integration of innovative digital health solutions to improve patient engagement</h3>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">-->
<!--                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
<!--                    <span class="sr-only">Previous</span>-->
<!--                </a>-->
<!--                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">-->
<!--                    <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
<!--                    <span class="sr-only">Next</span>-->
<!--                </a>-->
            </div>
         </div>
     </section>
      <section>
          <div class="container p-5 consulting">
              <h3 class="warn-col"><i class="fa fa-stethoscope"></i> Medical Content</h3>
              <p class="paragraph extra-color">We provide an integrated approach to client strategy development by providing exquisitely-crafted & validated content tailored to your audience, that streamlines your vision for your brand and turns passive guests into engaged customers. </p>
              <a href="service-page#medical-content-page" class="btn btn-sm warn-bg text-light mb-lg-5">Learn more</a>
          </div>
          <div class="container p-5 mb-5 ">
              <h3 class="warn-col"><i class="fa fa-user-md"></i> Consulting</h3>
              <p  class="paragraph extra-color">As a client, you are provided exceptional clinical expertise to help devise an effective strategy for the adoption of your product or service in physician practices, hospitals, pharmacies and health-conscious households.</p >
              <a href="service-page#banner" class="btn btn-sm warn-bg text-light mb-lg-5">Learn more</a>
          </div>
      </section>
      <section id="form-section" class="p-2">
          <div class="container">
              <h5 class="mb-3 mt-2 text-center">Please fill out the form below to request services.</h5>

<?php echo do_shortcode( '[contact-form-7 id="92" title="contact us"]' ); ?>

          </div>
      </section>
<?php get_footer(); ?>