<footer>
    <div id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pb-5">
                    <h4 class="camelcase">Contact Us</h4>
                    <div class="row">
                        <div class="col-md-12 d-md-flex">
                            <div class="icon-content">
                                <img src="<?php echo get_template_directory_uri() ?>/resources/images/mapicon.png" alt="map-icon">
                            </div>
                            <P class="d-block float-right paragraph mt-3 extra-color">The Enterprise Center, Suite 229 <br>
                                1922 S Martin Luther King Jr. Dr., Winston Salem, NC
                            </P>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 d-md-flex">
                            <div class=" icon-content">
                                <img src="<?php echo get_template_directory_uri() ?>/resources/images/postcard.png" alt="map-icon">
                            </div>
                            <P class="d-block float-right paragraph mt-3 extra-color">contact@ranviagroup.com</P>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 d-md-flex">
                            <div class="icon-content">
                                <img src="<?php echo get_template_directory_uri() ?>/resources/images/phoneicon.png" alt="map-icon">
                            </div>
                            <P class="d-block float-right paragraph mt-3 extra-color">(336) 701-3606</P>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pb-5">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="foot">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><p class="import-font">Copyright &copy; <?php echo date('Y') ?>. Ranvia Group LLC, All rights reserved.</p></div>
            </div>
        </div>
    </div>

</footer>
<a id="back-to-top" href="#" class="btn btn-default btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="fa fa-home p-2 warn"></span></a>

<!-------------Javascript------------------>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/resources/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/resources/js/bootstrap.min.js"></script>
<script>
    function initMap() {
        var uluru = {lat: 36.082699, lng: -80.220542};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnRbei5qlHVY9e6OUJvesEpPTeX1pql8E&callback=initMap"></script>
<script src="<?php echo get_template_directory_uri() ?>/resources/js/main.js"></script>
<?php wp_footer() ?>
</body>
</html>