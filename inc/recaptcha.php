function wpcf7_recaptcha_callback_script() {
if ( ! wp_script_is( 'google-recaptcha', 'enqueued' ) ) {
return;
}

?>
<script type="text/javascript">
    var recaptchaWidgets = [];
    var recaptchaCallback = function() {
        var forms = document.getElementsByTagName('form');
        var pattern = /(^|\s)g-recaptcha(\s|$)/;

        for (var i = 0; i < forms.length; i++) {
            var divs = forms[i].getElementsByTagName('div');

            for (var j = 0; j < divs.length; j++) {
                var sitekey = divs[j].getAttribute('data-sitekey');

                if (divs[j].className && divs[j].className.match(pattern) && sitekey) {
                    var params = {
                        'sitekey': sitekey,
                        'theme': divs[j].getAttribute('data-theme'),
                        'type': divs[j].getAttribute('data-type'),
                        'size': divs[j].getAttribute('data-size'),
                        'tabindex': divs[j].getAttribute('data-tabindex')
                    };

                    var callback = divs[j].getAttribute('data-callback');

                    if (callback && 'function' == typeof window[callback]) {
                        params['callback'] = window[callback];
                    }

                    var expired_callback = divs[j].getAttribute('data-expired-callback');

                    if (expired_callback && 'function' == typeof window[expired_callback]) {
                        params['expired-callback'] = window[expired_callback];
                    }

                    var widget_id = grecaptcha.render(divs[j], params);
                    recaptchaWidgets.push(widget_id);
                    break;
                }
            }
        }
    }
</script>
<?php