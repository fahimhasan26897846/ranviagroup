$(window).on('load', function() {
    $(".preloader-container").fadeOut("slow");
});

$(document).ready(function(){
    $(window).scroll(function () {
        if($(this).scrollTop()>20){
            $('.navbar').addClass('navchanger')
        }else {
            $('.navbar').removeClass('navchanger')
        }

        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');

});


    