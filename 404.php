<?php get_header(); ?>

<section class="banner-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 offset-md-3">
                <h2 class="banner-head-content text-light text-center animated  fadeInDown">OPPS SOMETHING WENT WRONG.</h2>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-md-center">
                <h1 class="warn-color default-family m-5">Error 404</h1>
                <p class="paragraph m-5">You are trying to get wrong link.</p>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
