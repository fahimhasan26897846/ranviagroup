<?php /*Template Name: ABOUT*/ ?>
<?php get_header(); ?>
      <section class="banner-page">
        <div class="container">
          <div class="row align-items-center">
              <div class="col-md-6 offset-md-3">
            <h2 class="banner-head-content text-light text-center animated  fadeInDown">Our Story</h2>
              </div>
           </div>
         </div>
     </section>
      <section>
          <div class="container p-5 mb-5 consulting">
              <h3 class="moder-header">History</h3>
              <p class="paragraph extra-color">Ranvia Group L.L.C has it’s origins in the crowd consulting arena, starting out as a vehicle for the founders to leverage their immediate network of clinicians to provide clinical assessments of new medical products in development by biotech startups. After numerous liaisons within the medical and pharmaceutical industries, we have decided to take our experience in idea curation and product development to develop a platform where physicians can provide value input into the product development process. All with an emphasis on devising ways to use technology to help patients take charge of their health. We have grown organically to now provide consulting services revolving around scientific and medical content for medical apps, direct-to-patient products in telehealth; AI-driven, web-based diagnostics, and genomics. We also provide branding, strategy, research & marketing expertise for small-to-large size healthcare companies that produce healthcare-related consumer technology.</p>
          </div>
          <div class="container p-5 mb-5"><h3 class="moder-header">Mission and vision</h3>
          <p class="paragraph extra-color">We are comprised of clinical and business professionals who specialize in finding innovative ways to bridge the gap between the health and technology spaces. We believe that by critically examining the various components of the healthcare workflow, we can institute changes to help you build a better product for your target audience. Our teams routinely tackle problems with technical, business and clinical aspects. We are a privately-owned and funded independent firm and strive to provide evidence-based insights in patient behavior to better target the product development process.</p>
          </div>
      </section>
      <section class="about-consultants p-4" id="consultants-section">
          <div class="container justify-content-center">
          <h2 class="text-light text-center" STYLE="font-family: proximanova">Our Team</h2>
          </div>
      </section>
      <section>
          <div class="container">
              <div class="row pt-5">
                  <div class="col-md-6"><img src="<?php echo get_template_directory_uri()?>/resources/images/gulala.jpg" alt="EZERIOHA" height="340px" width="344" class="rounded-circle img-fluid mt-2 mb-5">
                      <h3 class="warn-color">M. EZERIOHA</h3>
                      <h5 class="camelcase">CEO and Managing Consultant - Content Lead</h5>
                      <div class="links d-flex">
                          <a href="https://www.linkedin.com/in/nnadozie-ezerioha-md-474ba514"><i class="fa fa-linkedin-square fa-1x pr-2"></i></a>
                          <a href="mailto:dozie@ranviagroup.com"><i class="fa fa-envelope fa-1x pr-2 text-danger"></i></a>
                      </div>
                      <p class="paragraph extra-color mb-5">Dr. Ezerioha is very passionate about how medical professionals can use technology to help their patients take better care of their various health issues. With a background in physics (B.Sc), Ezerioha completed several internships at several companies prior to medical school. This included 2 summers at Fermilab, the largest particle physics research facility in the United States</p></div>
                  <div class="col-md-6">
                      <img src="<?php echo get_template_directory_uri()?>/resources/images/ikwu.jpg" alt="EZERIOHA" height="340px" width="344" class="rounded-circle img-fluid mt-2 mb-5">
                      <h3 class="warn-color">G. IKWUEZUNMA</h3>
                      <h5>Digital Health Specialist - IT Lead</h5>
                      <div class="links d-flex">
                          <a href="https://www.linkedin.com/in/gini-ikwuezunma-9b818113"><i class="fa fa-linkedin-square fa-1x pr-2"></i></a>
                          <a href="mailto:gini@ranviagroup.com"><i class="fa fa-envelope fa-1x pr-2 text-danger"></i></a>
                      </div>
                      <p class="paragraph extra-color mb-5">Dr. Ikwuezunma completed 3 years of his Ob-Gyn residency prior to joining Ranvia Group LLC. He brings a cumulative experience that spans 7 continents and is aimed at providing advanced healthcare solutions. Gini will serve as our Ob-Gyn content expert, helping deliver evidence-based healthcare content aimed at both health providers and patients; while also spearheading the digital health projects on our new platform.</p>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12 text-center">
                      <img src="<?php echo get_template_directory_uri()?>/resources/images/george.jpg" alt="EZERIOHA" height="340px" width="344" class="rounded-circle mt-2 mb-5 img-fluid">
                      <h3 class="warn-color">K. GEORGE</h3>
                      <h5>Consultant - Startup Advising</h5>
                      <div class="links">
                          <a href="mailto:contact@ranviagroup.com"><i class="fa fa-envelope fa-1x pr-2 text-danger"></i></a>
                      </div>
                      <p class="paragraph extra-color mb-5">Mr. George is a banking and finance professional with over 10 years of experience in startup consulting, credit analysis, loan syndication and portfolio administration. He is active in the local community and hopes to use his expertise in advising biotech startups in the triad area and beyond</p>
                  </div>
              </div>
          </div>
      </section>
      <section class="experts-sections-content p-5">
          <div class="container">
              <h3 class="warn-col">Content Experts</h3>
              <ul>
                  <li>♦ Psychiatry</li>
                  <li>♦ Radiology, AI and Machine Learning</li>
                  <li>♦ PM & R</li>
                  <li>♦ Connected Health</li>
                  <li>♦ Genomics and Personalized Medicine</li>
                  <li>♦ Augmented/Virtual Reality and Medical Device Interfaces</li>
              </ul>
          </div>
      </section>
      <section class="corporate-clients p-5">
          <div class="continer text-center">
              <h3 class="warn-col">Corporate Clients</h3>
              <div class="row mb-5">
                  <div class="col-md-6"><img  class="img-fluid mb-4"  src="<?php echo get_template_directory_uri()?>/resources/images/merch.png" alt="merch"></div>
                  <div class="col-md-6"><img class="img-fluid mb-4"  src="<?php echo get_template_directory_uri()?>/resources/images/johnson.png" alt=""></div>
              </div>
              <h3 class="warn-col">Startups</h3>
              <div class="row mb-5">
                  <div class="col-md-3"></div>
                  <div class="col-md-3"><img  class="img-fluid mb-4"  src="<?php echo get_template_directory_uri()?>/resources/images/howdish.png" alt="howdish"></div>
                  <div class="col-md-3"><img class="img-fluid mb-4"  src="<?php echo get_template_directory_uri()?>/resources/images/doctor.png" alt="doctor"></div>
                  <div class="col-md-3"></div>
              </div>
          </div>
      </section>
<?php get_footer(); ?>